package org.glue.fuse.common.processor;

import org.apache.camel.Exchange;
import org.glue.fuse.common.exception.NoDataDeducedException;
import org.glue.fuse.common.interfaces.AuditMessage;
import org.glue.fuse.common.interfaces.AuditMessageBuilderComponent;
import org.glue.fuse.common.interfaces.ErrorMessageBuilderComponent;
import org.glue.fuse.common.model.ErrorMessage;
import org.springframework.core.annotation.Order;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;

/**
 * @author Dan Miles
 */
@Component
@Order(value = 101)
public class SoapProcessor implements ErrorMessageBuilderComponent, AuditMessageBuilderComponent {

    private Jaxb2Marshaller serializer;
    private Exchange exchange;

    @Override
    public org.glue.fuse.common.model.DefaultRequestAuditMessage buildAuditMessage(AuditMessage auditMessage) throws ClassCastException {
        try {
            org.glue.fuse.common.model.DefaultRequestAuditMessage message = (org.glue.fuse.common.model.DefaultRequestAuditMessage) auditMessage;
            if (message.getRequestBody() == null) {
                message.setRequestBody(makeMessage());
            }
            return message;
        } catch (ClassCastException e){
            throw new RuntimeException("Cannot use this builder with this message");
        }
    }

    @Override
    public ErrorMessage buildErrorMessage(ErrorMessage errorMessage) {
        if (errorMessage.getRequestBody() == null) {
            errorMessage.setRequestBody(makeMessage());
        }
        return errorMessage;
    }

    private String makeMessage(){
        if (this.serializer != null) {
            return this.exchange.getIn().getBody().toString(); //TODO implement
        }
        return null;
    }

    @Override
    public void makeDatasource(Exchange exchange) throws NoDataDeducedException {
        if (exchange.getIn().getHeader("serializer") instanceof Jaxb2Marshaller) {
            this.serializer = (Jaxb2Marshaller) exchange.getIn().getHeader("serializer");
        }
        this.exchange = exchange;
    }

    @Override
    public Object getDatasource() {
        return exchange;
    }

    @Override
    public void setDatasource(Object datasource) {
        this.exchange = (Exchange) datasource;
    }
}
