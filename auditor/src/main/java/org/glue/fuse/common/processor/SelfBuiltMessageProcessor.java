package org.glue.fuse.common.processor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.Exchange;
import org.glue.fuse.common.exception.NoDataDeducedException;
import org.glue.fuse.common.interfaces.AuditMessage;
import org.glue.fuse.common.interfaces.AuditMessageBuilderComponent;
import org.glue.fuse.common.interfaces.ErrorMessageBuilderComponent;
import org.glue.fuse.common.interfaces.SelfBuiltMessage;
import org.glue.fuse.common.model.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author Dan Miles
 */
@Component
@Order(value = 80)
@Deprecated
public class SelfBuiltMessageProcessor implements AuditMessageBuilderComponent, ErrorMessageBuilderComponent {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private Exchange exchangeDatasource;
    private ObjectMapper serializer;

    private String createMessage(){
        if (this.exchangeDatasource != null && this.serializer != null){
            try {
                if (this.exchangeDatasource.getIn().getBody() instanceof SelfBuiltMessage) {
                    SelfBuiltMessage body = (SelfBuiltMessage) this.exchangeDatasource.getIn().getBody();
                    if (this.serializer != null) {
                        return this.serializer.writeValueAsString(body);
                    } else {
                        return "An error occurred during serialization";
                    }
                }
            } catch (JsonProcessingException | ClassCastException e) {
                LOGGER.debug(e.getMessage());
            }
        }
        return null;
    }

    @Override
    public org.glue.fuse.common.model.DefaultRequestAuditMessage buildAuditMessage(AuditMessage auditMessage) throws ClassCastException {
        try {
            org.glue.fuse.common.model.DefaultRequestAuditMessage message = (org.glue.fuse.common.model.DefaultRequestAuditMessage) auditMessage;
            if (message.getRequestBody() == null) {
                message.setRequestBody(createMessage());
            }
            return message;
        } catch (ClassCastException e){
            throw new RuntimeException("Cannot use this builder with this audit message");
        }
    }


    @Override
    public ErrorMessage buildErrorMessage(ErrorMessage errorMessage) {
        if (errorMessage.getRequestBody() == null){
            errorMessage.setRequestBody(createMessage());
        }
        return errorMessage;
    }

    @Override
    public void makeDatasource(Exchange exchange) throws NoDataDeducedException {
        try{
            if (exchange.getIn().getBody() instanceof ObjectMapper) {
                this.serializer = (ObjectMapper) exchange.getIn().getHeader("serializer");
            }
            this.exchangeDatasource = exchange;
        } catch (RuntimeException e){
            LOGGER.debug(e.getMessage());
            throw new NoDataDeducedException(e.getMessage());
        }
    }

    @Override
    public Object getDatasource() {
        return this.exchangeDatasource;
    }

    @Override
    public void setDatasource(Object datasource) {
        this.exchangeDatasource = (Exchange) datasource;
    }

}
