package org.glue.fuse.common;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

/**
 * @author Dan Miles
 */
@Component
public class AsyncAuditorCallbackFetcher {

    private static Logger LOGGER = LoggerFactory.getLogger(AsyncAuditorCallbackFetcher.class.getName());

    public static void getResponseInBody(Exchange exchange) {
        finishAudit(exchange);
        LOGGER.debug("Retrieved async response " + exchange.getProperty("response", Object.class));
        exchange.getIn().setBody(exchange.getProperty("response", String.class));
    }
    public static void getResponseInProperty(Exchange exchange) {
        finishAudit(exchange);
        LOGGER.debug("Retrieved async response " + exchange.getProperty("response", Object.class));
    }
    public static Boolean hasResponse(Exchange exchange) {
        finishAudit(exchange);
        LOGGER.debug("Retrieved async response " + exchange.getProperty("response", Object.class));
        return exchange.getProperty("response", Boolean.class);
    }

    public static Boolean isAudited(Exchange exchange) {
        finishAudit(exchange);
        return exchange.getProperty("audited", Boolean.class);
    }

    public static void finishAudit(Exchange exchange) {
        CountDownLatch countDownLatch = exchange.getProperty("countDownLatch", CountDownLatch.class);
        exchange.getContext().getAsyncProcessorAwaitManager().await(exchange, countDownLatch);
    }

}
