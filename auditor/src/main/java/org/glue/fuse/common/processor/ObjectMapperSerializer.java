package org.glue.fuse.common.processor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.Exchange;
import org.glue.fuse.common.exception.NoDataDeducedException;
import org.glue.fuse.common.interfaces.AuditMessage;
import org.glue.fuse.common.interfaces.AuditMessageBuilderComponent;
import org.glue.fuse.common.interfaces.ErrorMessageBuilderComponent;
import org.glue.fuse.common.model.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author Dan Miles
 */
@SuppressWarnings("Duplicates")
@Component
public class ObjectMapperSerializer implements AuditMessageBuilderComponent, ErrorMessageBuilderComponent {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private Exchange exchange;
    private ObjectMapper serializer;

    private String makeMessage() throws JsonProcessingException {
        return this.serializer.writeValueAsString(this.exchange.getIn().getBody());
    }

    @Override
    public AuditMessage buildAuditMessage(AuditMessage auditMessage) throws ClassCastException {
        try{
            org.glue.fuse.common.model.DefaultRequestAuditMessage message = (org.glue.fuse.common.model.DefaultRequestAuditMessage) auditMessage;
            if (message.getRequestBody() == null) {
                LOGGER.info("Building Audit message");
                message.setRequestBody(makeMessage());
            }
            return message;
        } catch (ClassCastException e){
            throw new RuntimeException("This builder cannot be used with this message");
        } catch (JsonProcessingException n) {
            LOGGER.warn(n.getMessage());
        }
        return auditMessage;
    }

    @Override
    public void makeDatasource(Exchange exchange) throws NoDataDeducedException {
        this.setDatasource(exchange);
    }

    @Override
    public Object getDatasource() {
        return this.exchange;
    }

    @Override
    public void setDatasource(Object datasource) {
        this.exchange = (Exchange) datasource;
        try {
            this.serializer = (ObjectMapper) this.exchange.getIn().getHeader("serializer");
        } catch (ClassCastException e){
            LOGGER.warn("Failed to get serializer, building default");
            try {
                this.serializer = new ObjectMapper();
            } catch (Exception n){
                throw new RuntimeException("Failed to build serializer after exception");
            }
        }
    }

    @Override
    public ErrorMessage buildErrorMessage(ErrorMessage errorMessage) {
        if (errorMessage.getRequestBody() == null) {
            LOGGER.info("Building Error Message");
            try {
                errorMessage.setRequestBody(makeMessage());
            } catch (JsonProcessingException e) {
                LOGGER.warn(e.getMessage());
            }
        }
        return errorMessage;
    }
}
