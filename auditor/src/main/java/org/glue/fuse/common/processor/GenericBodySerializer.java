package org.glue.fuse.common.processor;

import com.google.gson.Gson;
import org.apache.camel.Exchange;
import org.glue.fuse.common.exception.NoDataDeducedException;
import org.glue.fuse.common.interfaces.AuditMessage;
import org.glue.fuse.common.interfaces.AuditMessageBuilderComponent;
import org.glue.fuse.common.interfaces.ErrorMessageBuilderComponent;
import org.glue.fuse.common.model.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author Dan Miles
 */
@Component
@Order(value = 100)
public class GenericBodySerializer implements ErrorMessageBuilderComponent, AuditMessageBuilderComponent {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private final Gson gson = new Gson();
    private Exchange exchange;

    private synchronized String makeMessage(){
        return this.gson.toJson(this.exchange.getIn().getBody());
    }

    @Override
    public org.glue.fuse.common.model.DefaultRequestAuditMessage buildAuditMessage(AuditMessage auditMessage) throws ClassCastException {
        try{
        org.glue.fuse.common.model.DefaultRequestAuditMessage message = (org.glue.fuse.common.model.DefaultRequestAuditMessage) auditMessage;
        if (message.getRequestBody() == null) {
            LOGGER.info("Building Audit message");
            message.setRequestBody(makeMessage());
        }
        return message;
        } catch (ClassCastException e){
            throw new RuntimeException("This builder cannot be used with this message");
        }
    }

    @Override
    public ErrorMessage buildErrorMessage(ErrorMessage errorMessage) {
        if (errorMessage.getRequestBody() == null) {
            LOGGER.info("Building Error Message");
            errorMessage.setRequestBody(makeMessage());
        }
        return errorMessage;
    }

    @Override
    public void makeDatasource(Exchange exchange) throws NoDataDeducedException {
        this.exchange = exchange;
    }

    @Override
    public Object getDatasource() {
        return this.exchange;
    }

    @Override
    public void setDatasource(Object datasource) {
        this.exchange = (Exchange) datasource;
    }
}
