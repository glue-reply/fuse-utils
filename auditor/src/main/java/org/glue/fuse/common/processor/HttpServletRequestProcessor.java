package org.glue.fuse.common.processor;

import org.apache.camel.Exchange;
import org.glue.fuse.common.exception.NoDataDeducedException;
import org.glue.fuse.common.interfaces.AuditMessage;
import org.glue.fuse.common.interfaces.AuditMessageBuilderComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Dan Miles
 */
@Component
@Order(value = 2)
public class HttpServletRequestProcessor implements AuditMessageBuilderComponent {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private HttpServletRequest datasource;

    public HttpServletRequestProcessor(){
    }

    @Override
    public Object getDatasource() {
        return datasource;
    }

    @Override
    public void setDatasource(Object datasource) {
        this.datasource = (HttpServletRequest) datasource;
    }

    @Override
    public void makeDatasource(Exchange exchange) throws NoDataDeducedException {
        try {
            this.datasource = (HttpServletRequest) exchange.getIn().getHeader("request");
        } catch (ClassCastException e){
            throw new NoDataDeducedException(e.getMessage());
        }
    }

    @Override
    public org.glue.fuse.common.model.DefaultRequestAuditMessage buildAuditMessage(AuditMessage auditMessage) throws ClassCastException{
        try {
            org.glue.fuse.common.model.DefaultRequestAuditMessage message = (org.glue.fuse.common.model.DefaultRequestAuditMessage) auditMessage;
            if (this.datasource == null) {
                return message;
            }
            try {
                if (this.datasource.getSession().getId() != null && message.getSessionId() == null) {
                    message.setSessionId(this.datasource.getSession().getId());
                }
            } catch (RuntimeException e) {
                LOGGER.debug(e.getLocalizedMessage());
            }
            try {
                if (this.datasource.getRequestURI() != null && message.getRequestUrl() == null) {
                    message.setRequestUrl(this.datasource.getRequestURI());
                }
            } catch (RuntimeException e) {
                LOGGER.debug(e.getLocalizedMessage());
            }
            try {
                if (this.datasource.getUserPrincipal().getName() != null && message.getUserName() == null) {
                    message.setUserName(this.datasource.getUserPrincipal().getName());
                }
            } catch (RuntimeException e) {
                LOGGER.debug(e.getLocalizedMessage());
            }
            return message;
        } catch (ClassCastException e){
            throw new RuntimeException("This builder cannot be used with this message");
        }
    }
}
