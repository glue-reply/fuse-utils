package org.glue.fuse.common;

import org.apache.camel.Exchange;
import org.glue.fuse.common.exception.NoDataDeducedException;
import org.glue.fuse.common.interfaces.AuditMessage;
import org.glue.fuse.common.interfaces.AuditMessageBuilderComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author Dan Miles
 */
@Component
public class AuditMessageBuilder {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private final AuditMessageBuilderComponent[] auditMessageBuilders;
    private final Class<AuditMessage> auditMessageClass;

    @Autowired
    public AuditMessageBuilder(@Qualifier("chosenAuditors") AuditMessageBuilderComponent[] auditMessageBuilders,
                               @Qualifier("auditMessage") Class<AuditMessage> auditMessageClass){
        this.auditMessageBuilders = auditMessageBuilders;
        this.auditMessageClass = auditMessageClass;
    }

    /**
     * Contains simple logic to iterate builders and return audit Message
     * @param exchange
     * @return new AuditMessage
     */
    protected AuditMessage buildMessage(Exchange exchange) throws IllegalAccessException,
            InstantiationException {
        LOGGER.debug("building with: " + Arrays.toString(this.auditMessageBuilders));
        AuditMessage auditMessage = this.auditMessageClass.newInstance();
        if (this.auditMessageBuilders != null && this.auditMessageBuilders.length > 0){
            for (AuditMessageBuilderComponent messageBuilder : auditMessageBuilders){
                if (!auditMessage.isBuilt()) {
                    if (messageBuilder != null) {
                        try {
                            messageBuilder.makeDatasource(exchange);
                            messageBuilder.buildAuditMessage(auditMessage);
                        } catch (NoDataDeducedException e) {
                            LOGGER.debug(e.getMessage());
                        }
                    }
                } else {
                    return auditMessage;
                }
            }
        }
        LOGGER.debug("built auditMessage" + auditMessage.toString());
        return auditMessage;
    }

}
