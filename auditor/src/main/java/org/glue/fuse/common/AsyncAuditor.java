package org.glue.fuse.common;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.*;
import org.glue.fuse.common.interfaces.AuditMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class AsyncAuditor implements AsyncProcessor, CamelContextAware {

    private final ExecutorService executorService = Executors.newFixedThreadPool(20);
    private static Logger LOGGER = LoggerFactory.getLogger(AsyncAuditor.class.getName());
    private final AuditMessageBuilder auditMessageBuilder;
    private CamelContext camelContext;
    private final ObjectMapper serializer;

    @Value("${integration.audit.endpoint}")
    String auditEndpoint;

    @Value("${integration.audit.fail_on_incomplete}")
    String failOnIncomplete;
    @EndpointInject(uri = "log:out")
    ProducerTemplate producer;

    @Autowired
    public AsyncAuditor(AuditMessageBuilder auditMessageBuilder, ObjectMapper serializer) {
        this.auditMessageBuilder = auditMessageBuilder;
        this.serializer = serializer;
    }

    @Override
    public boolean process(final Exchange exchange, AsyncCallback asyncCallback) {
        LOGGER.info("Async process started");
        CountDownLatch countDownLatch = new CountDownLatch(1);
        exchange.setProperty("countDownLatch", countDownLatch);
        executorService.submit(new AsyncBackgroundProcess(exchange, b -> {
            LOGGER.info("Async backend process fininshed");
            exchange.getContext().getAsyncProcessorAwaitManager().countDown(exchange,
                    exchange.getProperty("countDownLatch", CountDownLatch.class));
        }));
        return true;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        throw new IllegalStateException("Should never be called");
    }

    @Override
    public void setCamelContext(CamelContext camelContext) {
        this.camelContext = camelContext;
    }

    @Override
    public CamelContext getCamelContext() {
        return this.camelContext;
    }

    private class AsyncBackgroundProcess implements Runnable {

        private Exchange exchange;
        private AsyncCallback asyncCallback;

        public AsyncBackgroundProcess(Exchange exchange, AsyncCallback asyncCallback){
            this.exchange = exchange;
            this.asyncCallback = asyncCallback;
        }

        @Override
        public void run() {
            LOGGER.info("Auditor async background process started");
            try {
                AuditMessage auditMessage = auditMessageBuilder.buildMessage(exchange);
                if (auditMessage.isBuilt()){
                    sendMessage(auditMessage);
                } else if (Objects.equals(failOnIncomplete, "no")){
                    sendMessage(auditMessage);
                } else {
                    exchange.setProperty("audited", false);
                    exchange.setProperty("response", auditMessage);
                    exchange.setException(new AuditorException("Audit Message was not built and configured to throw"));
                }
            } catch (IllegalAccessException | InstantiationException e) {
                LOGGER.warn("Failed to build Audit Document");
            }
            asyncCallback.done(false);
        }

        private void sendMessage(AuditMessage auditMessage){
            exchange.setProperty("audited", true);
            exchange.setProperty("response", auditMessage);
            try {
                producer.sendBody(auditEndpoint, serializer.writeValueAsString(auditMessage));
            } catch (JsonProcessingException e) {
                LOGGER.warn(e.getMessage());
                exchange.setProperty("audited", false);
                exchange.setProperty("response", auditMessage);
            }
        }

    }



}