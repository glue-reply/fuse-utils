package org.glue.fuse.common;

/**
 * @author Dan Miles
 */
public class AuditorException extends Exception {

    public AuditorException(String message){
        super(message);
    }

}
