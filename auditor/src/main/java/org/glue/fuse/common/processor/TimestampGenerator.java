package org.glue.fuse.common.processor;

import org.apache.camel.Exchange;
import org.glue.fuse.common.exception.NoDataDeducedException;
import org.glue.fuse.common.interfaces.AuditMessage;
import org.glue.fuse.common.interfaces.AuditMessageBuilderComponent;
import org.glue.fuse.common.interfaces.ErrorMessageBuilderComponent;
import org.glue.fuse.common.model.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

/**
 * @author Dan Miles
 */
@Component
@Order(value = 1)
public class TimestampGenerator implements AuditMessageBuilderComponent, ErrorMessageBuilderComponent {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private OffsetDateTime offsetDateTime;

    @Override
    public org.glue.fuse.common.model.DefaultRequestAuditMessage buildAuditMessage(AuditMessage auditMessage) throws ClassCastException {
        try {
            org.glue.fuse.common.model.DefaultRequestAuditMessage message = (org.glue.fuse.common.model.DefaultRequestAuditMessage) auditMessage;
            message.setApiTime(this.offsetDateTime);
            return message;
        } catch (ClassCastException e){
            throw new RuntimeException("Cannot use this builder with this message type");
        }
    }

    @Override
    public Object getDatasource() {
        return offsetDateTime;
    }

    @Override
    public void setDatasource(Object datasource) {
        this.offsetDateTime = (OffsetDateTime) datasource;
    }

    @Override
    public ErrorMessage buildErrorMessage(ErrorMessage errorMessage) {
        errorMessage.setApiTime(this.offsetDateTime);
        return errorMessage;
    }

    @Override
    public void makeDatasource(Exchange exchange) throws NoDataDeducedException {
        try {
            if (exchange.getIn().getHeader("auditTime") == null) {
                this.offsetDateTime = OffsetDateTime.now();
                exchange.getIn().setHeader("auditTime", this.offsetDateTime);
            } else {
                if (exchange.getIn().getHeader("auditTime") instanceof OffsetDateTime) {
                    this.offsetDateTime = (OffsetDateTime) exchange.getIn().getHeader("auditTime");
                }
            }
        }catch (RuntimeException e){
            LOGGER.debug(e.getMessage());
            throw new NoDataDeducedException(e.getMessage());
        }
    }
}
