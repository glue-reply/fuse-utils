package org.glue.fuse.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.*;
import org.glue.fuse.common.interfaces.AuditMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author Dan Miles
 */
@Component
public class Auditor implements Processor, CamelContextAware {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private CamelContext camelContext;
    private final AuditMessageBuilder builder;
    private final ObjectMapper serializer;

    @Value("${integration.audit.endpoint}")
    String auditEndpoint;

    @Value("${integration.audit.fail_on_incomplete}")
    String failOnIncomplete;
    @EndpointInject(uri = "log:out")
    ProducerTemplate producer;

    @Autowired
    public Auditor(AuditMessageBuilder builder, ObjectMapper serializer){
        this.builder = builder;
        this.serializer = serializer;
    }

    public void process(Exchange exchange) throws Exception {
        AuditMessage auditMessage = this.buildMessage(exchange);
        if (auditMessage != null) {
            if (this.sendMessage(auditMessage)) {
                exchange.setProperty("audited", true);
            } else {
                exchange.setProperty("audited", false);
            }
        }
    }

    private Boolean sendMessage(AuditMessage auditMessage) throws AuditorException, JsonProcessingException {
        if (auditMessage.isBuilt()) {
            producer.sendBody(this.auditEndpoint, this.serializer.writeValueAsString(auditMessage));
            return true;
        } else{
            if (Objects.equals(failOnIncomplete, "no")) {
                producer.sendBody(this.auditEndpoint, this.serializer.writeValueAsString(auditMessage));
                return false;
            } else{
                throw new AuditorException("Audit Message has not been fully built" +
                        " and is configured to throw.  You can change this behaviour in " +
                        "application.yml");
            }
        }
    }

    private AuditMessage buildMessage(Exchange exchange){
        AuditMessage auditMessage = null;
        try {
            auditMessage = this.builder.buildMessage(exchange);
        } catch (IllegalAccessException | InstantiationException e) {
            LOGGER.error(e.getLocalizedMessage());
        }
        return auditMessage;
    }

    @Override
    public void setCamelContext(CamelContext camelContext) {
        this.camelContext = camelContext;
    }

    @Override
    public CamelContext getCamelContext() {
        return this.camelContext;
    }
}