package org.glue.fuse.common;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.glue.fuse.common.interfaces.AuditMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author Dan Miles
 */
@Component
public class WiretapAuditor implements Processor {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private final AuditMessageBuilder auditMessageBuilder;

    @Value("${integration.audit.fail_on_incomplete}")
    String failOnIncomplete;


    @Autowired
    public WiretapAuditor(AuditMessageBuilder builder){
        this.auditMessageBuilder = builder;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        AuditMessage auditMessage = this.buildMessage(exchange);
        if (auditMessage.isBuilt()){
            exchange.setProperty("audited", true);
            exchange.getIn().setBody(auditMessage);
        } else {
            if (Objects.equals(failOnIncomplete, "no")) {
                exchange.setProperty("audited", true);
                exchange.getIn().setBody(auditMessage);
            } else{
                throw new AuditorException("Audit Message has not been fully built" +
                        " and is configured to throw.  You can change this behaviour in " +
                        "application.yml");
            }
        }
    }

    private AuditMessage buildMessage(Exchange exchange){
        AuditMessage auditMessage = null;
        try {
            auditMessage = this.auditMessageBuilder.buildMessage(exchange);
        } catch (IllegalAccessException | InstantiationException e) {
            LOGGER.error(e.getLocalizedMessage());
        }
        return auditMessage;
    }
}
