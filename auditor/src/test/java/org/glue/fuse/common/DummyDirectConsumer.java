package org.glue.fuse.common;

import org.apache.camel.Consume;
import org.glue.fuse.common.model.DefaultRequestAuditMessage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Dan Miles
 */
@Component
public class DummyDirectConsumer {

    @Value("${application.route.auditor.default}")
    String auditDefaultUri;

    @Consume(property = "auditDefaultUri")
    public void logMessages(DefaultRequestAuditMessage message){
        System.out.println(message);
    }

}
