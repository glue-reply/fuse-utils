package org.glue.fuse.common.translator.cache;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author Dan Miles
 */
@Deprecated
public class RedisCache implements CacheManager {

    private static JedisPool connectionPool;
    private String host;

    public RedisCache(String host){
        connectionPool = new JedisPool(new JedisPoolConfig(), host);
    }

    public RedisCache(){
        connectionPool = new JedisPool(new JedisPoolConfig(), "localhost");
    }

    public String getKeyValue(String key) {
        try (Jedis jedis = connectionPool.getResource()){
            return jedis.get(key);
        } catch (RuntimeException e){
            e.printStackTrace();
            return null;
        }

    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void connectToCache(){
        connectionPool = new JedisPool(new JedisPoolConfig(), this.host);
    }
}
