package org.glue.fuse.common;

import org.glue.fuse.common.interfaces.ExchangeEnrichedVisitor;
import org.glue.fuse.common.translator.TranslationLookupClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Extends the ExchangeEnrichedVisitor, abstracting the required methods and attributed required.
 *
 * Extend this class with a method name following the convention`visit${classname}`
 * @author Dan Miles
 */
@Component
public abstract class TranslationLookupVisitor extends ExchangeEnrichedVisitor {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private final DiscoveryClient discoveryClient;
    private TranslationLookupClient lookupClient;

    @Autowired
    public TranslationLookupVisitor(DiscoveryClient discoveryClient) {
        this.discoveryClient = discoveryClient;
        this.build();
    }

    @Override
    public void visit(Object object){
        try{
            getMethod(object.getClass()).invoke(this, object);
        } catch (Exception e){
            LOGGER.warn(e.getLocalizedMessage());
        }
    }

    private void build(){
        List<ServiceInstance> services = this.getServices();
        this.lookupClient = new TranslationLookupClient(services.get(0).getHost(), services.get(0).getPort());
    }

    private List<ServiceInstance> getServices() {
        // TODO probably inject the service name by configuration or something
        List<ServiceInstance> list = this.discoveryClient.getInstances("TranslationLookupService");
        if (list != null && list.size() > 0 ) {
            return list;
        }
        return null;
    }

    public TranslationLookupClient getLookupClient() {
        return lookupClient;
    }
}