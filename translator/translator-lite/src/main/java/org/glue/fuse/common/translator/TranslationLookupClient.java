package org.glue.fuse.common.translator;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * @author Dan Miles
 */
public class TranslationLookupClient {

    private static Logger LOGGER = LoggerFactory.getLogger(TranslationLookupClient.class.getName());

    private final ManagedChannel managedChannel;
    private final org.glue.fuse.common.translator.ValueLookupGrpc.ValueLookupBlockingStub blockingStub;

    public TranslationLookupClient(String host, int port){
        this(ManagedChannelBuilder.forAddress(host, port).build());
    }

    TranslationLookupClient(ManagedChannel channel){
        this.managedChannel = channel;
        this.blockingStub = org.glue.fuse.common.translator.ValueLookupGrpc.newBlockingStub(channel);
    }

    public void shutdown() throws InterruptedException {
        this.managedChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public String translate(String key, String dataset){

        org.glue.fuse.common.translator.ValueLookupRequest request =
                org.glue.fuse.common.translator.ValueLookupRequest.newBuilder().
                        setValue(key).setDataset(dataset).build();

        org.glue.fuse.common.translator.ValueResponse response;
        try {
            response = this.blockingStub.translateValue(request);
        } catch (StatusRuntimeException e){
            LOGGER.warn("RPC Failed " + e.getStatus());
            return null;
        }
        return response.getResponse();

    }

}
