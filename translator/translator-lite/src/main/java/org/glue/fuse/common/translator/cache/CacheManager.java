package org.glue.fuse.common.translator.cache;

/**
 * @author Dan Miles
 */
@Deprecated
public interface CacheManager {

    String getKeyValue(String key);

}
