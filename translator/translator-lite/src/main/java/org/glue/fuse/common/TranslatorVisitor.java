package org.glue.fuse.common;

import org.glue.fuse.common.interfaces.ExchangeEnrichedVisitor;
import org.glue.fuse.common.translator.cache.CacheFactory;
import org.glue.fuse.common.translator.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Extends the ExchangeEnrichedVisitor, abstracting the required methods and attributed required.
 *
 * Extend this class with a method name following the convention`visit${classname}`
 * @author Dan Miles
 */
@Component
public abstract class TranslatorVisitor extends ExchangeEnrichedVisitor {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private final CacheFactory cacheFactory;
    private CacheManager cacheManager;

    @Autowired
    public TranslatorVisitor(CacheFactory cacheFactory) {
        this.cacheFactory = cacheFactory;
        this.cacheManager = this.cacheFactory.buildCacheManager();
    }

    @Override
    public void visit(Object object){
        try{
            getMethod(object.getClass()).invoke(this, object);
        } catch (Exception e){
            LOGGER.warn(e.getLocalizedMessage());
        }
    }

    public CacheFactory getCacheFactory() {
        return cacheFactory;
    }

    public void setCacheManager(CacheManager cacheManager){
        this.cacheManager = cacheManager;
    }

    public CacheManager getCacheManager() {
        return this.cacheManager;
    }
}
