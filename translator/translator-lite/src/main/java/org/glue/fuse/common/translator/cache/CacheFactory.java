package org.glue.fuse.common.translator.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Dan Miles
 */
//TODO this needs additional config options
@Component
@Deprecated
public class CacheFactory {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private String hostname;
    @Value("${application.cache.provider}")
    private String cacheProvider;
    private CacheManager cacheManager;

    public CacheFactory(){
    }

    public CacheManager buildCacheManager(){
        try {
            switch (this.cacheProvider.toLowerCase()) {
                //add more managers here as we add more support
                case "redis":
                    if (this.hostname != null) {
                        return new RedisCache(this.hostname);
                    } else {
                        return new RedisCache();
                    }
                default:
                    return getDefaultProvider();
            }
        } catch (Exception e){
            LOGGER.debug("No cacheProvider given: " + e.getLocalizedMessage());
            try{
                return getDefaultProvider();
            } catch (RuntimeException n){
                LOGGER.debug("Encountered exception getting default provider: " + n.getMessage());
            }
        }
        return getDefaultProvider();
    }

    @SuppressWarnings("WeakerAccess")
    public CacheManager getCacheManager(){
        if (this.cacheManager == null){
            this.cacheManager = buildCacheManager();
            return this.cacheManager;
        } else {
            return this.cacheManager;
        }
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setCacheProvider(String cacheProvider) {
        this.cacheProvider = cacheProvider;
    }

    @SuppressWarnings("WeakerAccess")
    public CacheManager getDefaultProvider(){
        if (this.hostname != null) {
            return new RedisCache(this.hostname);
        } else{
            return new RedisCache();
        }
    }


}
