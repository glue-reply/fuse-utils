package org.glue.fuse.common.translator;

import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * A Spring Boot Service to run the translator server.
 * @Author Dan Miles
 */
@SpringBootApplication
public class TranslationLookupSpringService {

    public static void main(String[] args){
        SpringApplication.run(TranslationLookupSpringService.class);
    }

    @Autowired
    public TranslationLookupSpringService(TranslationHelper helper){
        this.helper = helper;
    }

    private final TranslationHelper helper;

    @GRpcService
    public class LookupService extends org.glue.fuse.common.translator.ValueLookupGrpc.ValueLookupImplBase {
        @Override
        public void translateValue(org.glue.fuse.common.translator.ValueLookupRequest request,
                                   StreamObserver<org.glue.fuse.common.translator.ValueResponse> responseObserver) {
            request.getValue();
            org.glue.fuse.common.translator.ValueResponse response =
                    org.glue.fuse.common.translator.ValueResponse.newBuilder().setResponse(
                            helper.translate(request.getValue(), request.getDataset()
                    )).build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();

        }
    }

}
