package org.glue.fuse.common.translator;

public interface TranslationHelper {

    String translate(String key, String dataset);

}
