package org.glue.fuse.common.translator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SimpleCachedTranslator implements TranslationHelper {

    private final Object cacheManager;
    private final Object persistentStore;

    @Autowired
    public SimpleCachedTranslator(Object cacheManager, Object persistentStore){
        this.cacheManager = cacheManager;
        this.persistentStore = persistentStore;
    }


    @Override
    public String translate(String key, String dataset) {
        // TODO implement
        return null;
    }


}
