package org.glue.fuse.common.translator;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Completely Standalone translation lookup server, runnable from the CLI
 * @author Dan Miles
 */
public class TranslationLookupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TranslationLookupService.class.getName());

    private Server server;

    private void start() throws IOException {
        /* The port on which the server should run */
        int port = 50051;
        server = ServerBuilder.forPort(port)
                .addService(new LookupImpl())
                .build()
                .start();
        LOGGER.info("Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.err.println("*** shutting down gRPC server since JVM is shutting down");
            TranslationLookupService.this.stop();
            System.err.println("*** server shut down");
        }));
    }

    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        final TranslationLookupService server = new TranslationLookupService();
        server.start();
        server.blockUntilShutdown();
    }

    static class LookupImpl extends org.glue.fuse.common.translator.ValueLookupGrpc.ValueLookupImplBase {

        @Override
        public void translateValue(org.glue.fuse.common.translator.ValueLookupRequest request,
                                   StreamObserver<org.glue.fuse.common.translator.ValueResponse> responseObserver) {
            request.getValue();
            // TODO implement the actual lookup values. This should be a separate class to DI into this service
            org.glue.fuse.common.translator.ValueResponse response =
                    org.glue.fuse.common.translator.ValueResponse.newBuilder().setResponse("TODO").build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();

        }
    }

}
