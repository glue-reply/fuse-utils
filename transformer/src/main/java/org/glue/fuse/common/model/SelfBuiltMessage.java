package org.glue.fuse.common.model;

import org.apache.camel.Exchange;

/**
 * @author Dan Miles
 */
public interface SelfBuiltMessage {

    SelfBuiltMessage build(Exchange exchange);

}
