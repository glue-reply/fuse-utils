package org.glue.fuse.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Dan Miles
 */
@Component
public class GenericSerializer extends ObjectMapperSerializer implements Processor, Serializer {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.info("Processing " + exchange.getExchangeId());
        this.setSerializer(exchange);
        Object body = exchange.getIn().getBody();
        if (body instanceof InputStream){
            exchange.getIn().setBody(deserializeInputStream((InputStream) body));
        } else {
            exchange.getIn().setBody(serializeObject(body));
        }
    }

    private String serializeObject(Object target) throws JsonProcessingException {
        return getSerializer().writeValueAsString(target);
    }

    private JsonNode deserializeInputStream(InputStream inputStream) throws IOException {
        JsonNode jsonNode = getSerializer().readTree(inputStream);
        return jsonNode;
    }

}
