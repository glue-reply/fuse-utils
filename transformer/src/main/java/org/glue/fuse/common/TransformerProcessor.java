package org.glue.fuse.common;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

/**
 * This should be able to serialize and de-serialize objects being sent to REST API's
 * It takes objects and converts them to JSON strings using either GSON or an ObjectMapper.
 *
 * De-serialization requires a target class.  This can also support taking an array of classes if there is likely to be
 * more than 1 type (Java object models, not just if the field is populated) of response.
 * @author Dan Miles
 */
@Deprecated
public class TransformerProcessor implements Processor, Serializer {

    private Gson gson;
    private ObjectMapper objectMapper;
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private Class<?> targetMessageClass;
    private Class<?>[] targetMessageClasses;

    public TransformerProcessor(Class<?> targetMessageClass) {
        this.targetMessageClass = targetMessageClass;
    }

    public TransformerProcessor(Class<?>[] targetMessageClasses){
        this.targetMessageClasses = targetMessageClasses;
    }

    public void process(Exchange exchange) throws Exception {
        Object serializer = exchange.getIn().getHeader("serializer"); //use the configured serializer if it's there
        if (serializer instanceof ObjectMapper){
            this.objectMapper = (ObjectMapper) serializer;
            useObjectMapper(exchange);
        } else if (serializer instanceof Gson){
            this.gson = (Gson) serializer;
            useGson(exchange);
        } else{ //fallback to a safe default: ObjectMapper
            this.objectMapper = new ObjectMapper();
            useObjectMapper(exchange);
        }
    }

    private void useGson(Exchange exchange) throws Exception {
        if (exchange.getIn().getBody() instanceof InputStream){
            InputStream body = (InputStream) exchange.getIn().getBody();
            exchange.getIn().setBody(deserializeWithGson(body));
        } else if (exchange.getIn().getBody() != null) {
            exchange.getIn().setBody(this.gson.toJson(exchange.getIn().getBody()));
        } else {
            throw new Exception("Failed to perform data transformation with Gson");
        }

    }

    private void useObjectMapper(Exchange exchange) throws Exception{
        if (exchange.getIn().getBody() instanceof InputStream) {
            InputStream body = (InputStream) exchange.getIn().getBody();
            exchange.getIn().setBody(deserializeWithObjectMapper(body));
        } else if (exchange.getIn().getBody() != null) {
            exchange.getIn().setBody(this.objectMapper.writeValueAsString(exchange.getIn().getBody()));
        } else {
            throw new Exception("Failed to perform data transformation with Object Mapper");
        }
    }

    private Object deserializeWithGson(InputStream body){
        if (this.targetMessageClass != null) {
            return this.gson.fromJson(String.valueOf(body), targetMessageClass);
        } else {
            for (Class<?> targetClass : this.targetMessageClasses){
                try {
                    return this.gson.fromJson(buildString(body), targetClass);
                } catch (Exception e){
                    LOGGER.debug(e.getMessage());
                }
            }
        }
        return null;
    }

    private Object deserializeWithObjectMapper(InputStream body){
        if (this.targetMessageClass != null) {
            try {
                return this.objectMapper.readValue(body, targetMessageClass);
            } catch (IOException e) {
                LOGGER.warn(e.getMessage());
            }
        } else {
            for (Class<?> targetClass : this.targetMessageClasses){
                try{
                    return this.objectMapper.readValue(body, targetClass);
                } catch (JsonParseException e) {
                    LOGGER.debug(e.getMessage());
                } catch (JsonMappingException e) {
                    LOGGER.debug(e.getMessage());
                } catch (IOException e) {
                    LOGGER.debug(e.getMessage());
                }
            }
        }
        return null;
    }

    private String buildString(InputStream inputStream) throws IOException {
        final int bufferSize = 1024;
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        Reader in = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        for (; ; ) {
            int rsz = in.read(buffer, 0, buffer.length);
            if (rsz < 0)
                break;
            out.append(buffer, 0, rsz);
        }
        return out.toString();
    }

}
