package org.glue.fuse.common;

import org.glue.fuse.common.interfaces.ExchangeEnrichedVisitor;
import org.glue.fuse.common.interfaces.Visitable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Extends the ExchangeEnrichedVisitor, abstracting the required methods and attributed required.
 *
 * Extend this class with a method name following the convention`visit${classname}` and autowiring the target message
 * in the class constructor.  This should really be done with a qualifier and a config class.
 * @author Dan Miles
 */
@Component
public abstract class TransformerVisitor extends ExchangeEnrichedVisitor {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private final Class<Visitable> targetMessage;

    public TransformerVisitor(Class<Visitable> targetMessage) {
        this.targetMessage = targetMessage;
    }

    @Override
    public void visit(Object object){
        try{
            getMethod(object.getClass()).invoke(this, object);
        } catch (Exception e){
            LOGGER.warn(e.getLocalizedMessage());
        }
    }

    protected Visitable getTargetMessage() {
        try {
            return this.targetMessage.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

}
