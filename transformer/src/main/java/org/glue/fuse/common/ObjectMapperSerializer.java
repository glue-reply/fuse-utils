package org.glue.fuse.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.Exchange;

/**
 * @author Dan Miles
 */
public class ObjectMapperSerializer {

    private ObjectMapper serializer;


    public void setSerializer(Exchange exchange){
        Object serializer = exchange.getIn().getHeader("serializer");
        if (serializer instanceof ObjectMapper){
            this.serializer = (ObjectMapper) serializer;
        } else if (serializer == null){
            this.serializer = new ObjectMapper();
        } else {
            throw new RuntimeException("Generic Serializer can only be used with a configured ObjectMapper");
        }
    }


    public void setSerializer(ObjectMapper serializer) {
        this.serializer = serializer;
    }

    public ObjectMapper getSerializer() {
        return this.serializer;
    }
}
