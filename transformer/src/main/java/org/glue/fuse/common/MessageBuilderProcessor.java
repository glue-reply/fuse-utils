package org.glue.fuse.common;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.glue.fuse.common.model.SelfBuiltMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Dan Miles
 */
@Deprecated
public class MessageBuilderProcessor implements Processor {
    private SelfBuiltMessage targetMessage;
    private SelfBuiltMessage[] targetMessages;
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    public MessageBuilderProcessor(SelfBuiltMessage targetMessage) {
        this.targetMessage = targetMessage;
    }

    public MessageBuilderProcessor(SelfBuiltMessage[] targetMessages){
        this.targetMessages = targetMessages;
    }

    public void process(Exchange exchange) {
        if (this.targetMessage != null) {
            exchange.getIn().setBody(buildMessage(exchange));
        } else{
            for (SelfBuiltMessage targetMessage : this.targetMessages){
                try{
                    this.targetMessage = targetMessage;
                    exchange.getIn().setBody(buildMessage(exchange));
                    break;
                }catch (ClassCastException e){
                    LOGGER.debug(e.getMessage());
                }
            }
        }
    }

    private SelfBuiltMessage buildMessage(Exchange exchange) throws ClassCastException{
        this.targetMessage.build(exchange);
        return this.targetMessage;
    }
}
