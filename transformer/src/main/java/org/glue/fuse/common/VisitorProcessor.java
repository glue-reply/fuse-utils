package org.glue.fuse.common;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.glue.fuse.common.interfaces.ExchangeEnrichedVisitor;
import org.glue.fuse.common.interfaces.Visitable;

/**
 * Acts as the processor wrapper around visitor components.  There is a base condition check for the transformers,
 * because it needs to get the target message from there for it to visit (This could be argued is a little backwards)
 *
 * The exchange for the ExchangeEnrichedVisitor is set at the start of the processing for access further down.
 *
 * @author Dan Miles
 */
public class VisitorProcessor implements Processor {

    private ExchangeEnrichedVisitor visitor;

    public VisitorProcessor(ExchangeEnrichedVisitor visitor) {
        this.visitor = visitor;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        visitor.setExchange(exchange);
        if (visitor instanceof TransformerVisitor){
            Visitable host = ((TransformerVisitor) visitor).getTargetMessage();
            host.accept(visitor);
        } else {
            Visitable host = (Visitable) exchange.getIn().getBody();
            host.accept(visitor);
        }
    }
}
