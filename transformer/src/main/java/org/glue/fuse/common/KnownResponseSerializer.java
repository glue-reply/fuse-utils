package org.glue.fuse.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Dan Miles
 */
public class KnownResponseSerializer extends ObjectMapperSerializer implements Processor, Serializer {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private final Class targetModel;

    public KnownResponseSerializer(Class targetModel) {
        this.targetModel = targetModel;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.info("Processing" + exchange.getExchangeId());
        this.setSerializer(exchange);
        Object body = exchange.getIn().getBody();
        if (body instanceof InputStream){
            exchange.getIn().setBody(deserializeInputStream((InputStream) body));
        } else {
            exchange.getIn().setBody(serializeObject(body));
        }
    }

    private String serializeObject(Object body) throws JsonProcessingException {
        return this.getSerializer().writeValueAsString(body);
    }

    private Object deserializeInputStream(InputStream body) throws IOException {
        return getSerializer().readValue(body, this.targetModel);
    }

}
