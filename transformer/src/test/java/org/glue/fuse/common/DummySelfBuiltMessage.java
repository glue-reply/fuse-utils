package org.glue.fuse.common;

import org.apache.camel.Exchange;
import org.glue.fuse.common.model.SelfBuiltMessage;

public class DummySelfBuiltMessage implements SelfBuiltMessage {

    private String name;
    private String description;

    public DummySelfBuiltMessage(){
        this.name = "A Dummy Class";
        this.description = "I'm just a dummy, be nice to me";
    }

    public DummySelfBuiltMessage(String name, String description){
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SelfBuiltMessage build(Exchange e) {
        this.description = "I built myself";
        return this;
    }
}
