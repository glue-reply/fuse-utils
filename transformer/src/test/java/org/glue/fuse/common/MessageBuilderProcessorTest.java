package org.glue.fuse.common;

import org.apache.camel.EndpointInject;
import org.apache.camel.Processor;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;

public class MessageBuilderProcessorTest extends CamelTestSupport {

    @EndpointInject(uri = "mock:result")
    protected MockEndpoint resultEndpoint;

    @Produce(uri = "direct:start")
    protected ProducerTemplate template;

    protected final DummySelfBuiltMessage target = new DummySelfBuiltMessage();
    protected final Processor singleTransformer = new MessageBuilderProcessor(target);

    @Test
    public void testSendMatchingMessage() throws Exception {
        resultEndpoint.expectedBodiesReceived(target);
        template.sendBodyAndHeader(target, "foo", "bar");
        resultEndpoint.assertIsSatisfied();
    }

    @Test
    public void testSendNotMatchingMessage() throws Exception {
        resultEndpoint.expectedMessageCount(0);
        template.sendBodyAndHeader("<notMatched/>", "foo", "notMatchedHeaderValue");
        resultEndpoint.assertIsSatisfied();
    }

    @Override
    protected RouteBuilder createRouteBuilder() {
        return new RouteBuilder() {
            public void configure() {
                from("direct:start")
                        .filter(header("foo")
                                .isEqualTo("bar"))
                        .process(singleTransformer)
                        .to("mock:result");
            }
        };
    }
}