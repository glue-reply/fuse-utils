package org.glue.fuse.common;

import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author Dan Miles
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ErrorHandlerTest extends CamelTestSupport {

    @EndpointInject(uri = "mock:result")
    protected MockEndpoint resultEndpoint;

    @Produce(uri = "direct:start")
    protected ProducerTemplate template;

    //TODO fix
    //protected Processor errorHandler = new ErrorHandler();

    @Test
    public void testSendMatchingMessage() throws Exception {
        String target = "<matched>";
        resultEndpoint.expectedBodiesReceived(target);
        template.sendBodyAndHeader(target, "foo", "bar");
        resultEndpoint.assertIsSatisfied();
    }

    @Test
    public void testSendNotMatchingMessage() throws Exception {
        resultEndpoint.expectedMessageCount(0);
        template.sendBodyAndHeader("<notMatched/>", "foo", "notMatchedHeaderValue");
        resultEndpoint.assertIsSatisfied();
    }

    @Override
    protected RouteBuilder createRouteBuilder() {
        return new RouteBuilder() {
            public void configure() {
                from("direct:start")
                        .filter(header("foo")
                                .isEqualTo("bar"))
                        //.process(errorHandler)
                        .to("mock:result");
            }
        };
    }
}