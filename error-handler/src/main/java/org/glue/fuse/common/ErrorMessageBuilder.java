package org.glue.fuse.common;

import org.apache.camel.Exchange;
import org.glue.fuse.common.exception.NoDataDeducedException;
import org.glue.fuse.common.interfaces.ErrorMessageBuilderComponent;
import org.glue.fuse.common.model.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Dan Miles
 */
@Component
public class ErrorMessageBuilder {

    private final ErrorMessageBuilderComponent[] errorMessageBuilders;
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    public ErrorMessageBuilder(ErrorMessageBuilderComponent[] errorMessageBuilders){
        this.errorMessageBuilders = errorMessageBuilders;
    }

    protected ErrorMessage buildMessage(Exchange exchange){
        ErrorMessage errorMessage = new ErrorMessage("Something went wrong in the ErrorHandler builders.  Are you sure"
                + "they were initialised?");
        if (this.errorMessageBuilders != null && this.errorMessageBuilders.length > 0) {
            for (ErrorMessageBuilderComponent builder : errorMessageBuilders) {
                if (builder != null) {
                    try {
                        builder.makeDatasource(exchange);
                        builder.buildErrorMessage(errorMessage);
                    } catch (NoDataDeducedException e) {
                        LOGGER.debug(e.getMessage());
                    }
                }
            }
        }
        return errorMessage;
    }

}
