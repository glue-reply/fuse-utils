package org.glue.fuse.common;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Dan Miles
 */
public class WiretapErrorHandler implements Processor {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private final ErrorMessageBuilder errorMessageBuilder;

    @Autowired
    public WiretapErrorHandler(ErrorMessageBuilder builder){
        this.errorMessageBuilder = builder;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        exchange.getIn().setBody(this.errorMessageBuilder.buildMessage(exchange));
    }
}
