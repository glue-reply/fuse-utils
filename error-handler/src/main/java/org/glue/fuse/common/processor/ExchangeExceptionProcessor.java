package org.glue.fuse.common.processor;

import org.apache.camel.Exchange;
import org.glue.fuse.common.exception.NoDataDeducedException;
import org.glue.fuse.common.interfaces.ErrorMessageBuilderComponent;
import org.glue.fuse.common.model.ErrorMessage;
import org.springframework.stereotype.Component;

/**
 * @author Dan Miles
 */
@Component
public class ExchangeExceptionProcessor implements ErrorMessageBuilderComponent {

    private Exception exchangeException;


    @Override
    public ErrorMessage buildErrorMessage(ErrorMessage errorMessage) {
        errorMessage.setException(this.exchangeException);
        errorMessage.setErrorMessage(this.exchangeException.getMessage());
        return errorMessage;
    }

    @Override
    public void makeDatasource(Exchange exchange) throws NoDataDeducedException {
        this.exchangeException = exchange.getException();
    }

    @Override
    public Object getDatasource() {
        return this.exchangeException;
    }

    @Override
    public void setDatasource(Object datasource) {
        this.exchangeException = (Exception) datasource;
    }
}
