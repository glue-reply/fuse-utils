package org.glue.fuse.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.*;
import org.glue.fuse.common.model.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Dan Miles
 */
@Component
public class ErrorHandler implements Processor, CamelContextAware {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    private final ErrorMessageBuilder messageBuilder;
    private CamelContext camelContext;
    private final ObjectMapper serializer;

    @EndpointInject(uri = "log:out")
    ProducerTemplate errorEndpoint;


    @Value("${integration.error.errorEndpointURI}")
    String errorEndpointURI;

    @Autowired
    public ErrorHandler(ErrorMessageBuilder builder, ObjectMapper serializer){
        this.messageBuilder = builder;
        this.serializer = serializer;
    }


    public void process(Exchange exchange) throws Exception {
        ErrorMessage errorMessage = this.messageBuilder.buildMessage(exchange);
        this.errorEndpoint.sendBody(errorEndpointURI, this.serializer.writeValueAsString(errorMessage));
        exchange.getIn().setHeader("errorDoc", errorMessage);
    }

    public void setCamelContext(CamelContext camelContext) {
        this.camelContext = camelContext;
    }

    public CamelContext getCamelContext() {
        return this.camelContext;
    }
}
