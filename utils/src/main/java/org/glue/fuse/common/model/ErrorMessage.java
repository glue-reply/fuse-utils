package org.glue.fuse.common.model;

import java.time.OffsetDateTime;

/**
 * @author Dan Miles
 * Simple Error Message POJO
 */
public class ErrorMessage {

    private String accessNetwork;
    private OffsetDateTime apiTime; //TimeStamp Generator
    private String requestBody; //SelfBuiltMessage Processor
    private Exception exception; //Exception Processor
    private String errorMessage; //Overwritten by Exception Processor

    public ErrorMessage(String message){
        this.errorMessage = message;
    }


    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public OffsetDateTime getApiTime() {
        return apiTime;
    }

    public void setApiTime(OffsetDateTime apiTime) {
        this.apiTime = apiTime;
    }

    public String getAccessNetwork() {
        return accessNetwork;
    }

    public void setAccessNetwork(String accessNetwork) {
        this.accessNetwork = accessNetwork;
    }
}
