package org.glue.fuse.common.interfaces;


/**
 * @author Dan Miles
 */
public interface AuditMessage {

    Boolean isBuilt();

}
