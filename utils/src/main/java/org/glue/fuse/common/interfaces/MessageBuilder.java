package org.glue.fuse.common.interfaces;

import org.apache.camel.Exchange;
import org.glue.fuse.common.exception.NoDataDeducedException;

/**
 * @author Dan Miles
 */
public interface MessageBuilder {

    void makeDatasource(Exchange exchange) throws NoDataDeducedException;

    Object getDatasource();

    void setDatasource(Object datasource);
}
