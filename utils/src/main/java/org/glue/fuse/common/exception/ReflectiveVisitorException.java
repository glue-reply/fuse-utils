package org.glue.fuse.common.exception;

/**
 * @author Dan Miles
 */
public class ReflectiveVisitorException extends Exception {

    public ReflectiveVisitorException(String message){
        super(message);
    }

}
