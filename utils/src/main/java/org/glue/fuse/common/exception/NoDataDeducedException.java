package org.glue.fuse.common.exception;

/**
 * @author Dan Miles
 */
public class NoDataDeducedException extends Exception {

    public NoDataDeducedException(String message){
        super(message);
    }
}
