package org.glue.fuse.common.interfaces;

import org.glue.fuse.common.model.ErrorMessage;

/**
 * @author Dan Miles
 */
public interface ErrorMessageBuilderComponent extends MessageBuilder {

    ErrorMessage buildErrorMessage(ErrorMessage errorMessage);

}
