package org.glue.fuse.common.model;

import org.glue.fuse.common.interfaces.ExchangeEnrichedVisitor;
import org.glue.fuse.common.interfaces.Visitable;

/**
 * @author Dan Miles
 */
public class DummyVisitable implements Visitable {

    private String message;

    public DummyVisitable(String message){
        this.message = message;
    }

    @Override
    public void accept(ExchangeEnrichedVisitor visitor) {
        visitor.visit(this);
    }

    public String dummyVisitable(){
        return "I'm a dummy";
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
