package org.glue.fuse.common.interfaces;

/**
 * @author Dan Miles
 */
public interface AuditMessageBuilderComponent extends MessageBuilder {

    AuditMessage buildAuditMessage(AuditMessage auditMessage) throws ClassCastException;

}
