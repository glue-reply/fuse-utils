package org.glue.fuse.common.interfaces;

/**
 * @author Dan Miles
 */
public interface Visitable {

    void accept(ExchangeEnrichedVisitor visitor);

}
