package org.glue.fuse.common.interfaces;

import org.apache.camel.Exchange;

/**
 * @author Dan Miles
 */
@Deprecated
public interface SelfBuiltMessage {

    SelfBuiltMessage build(Exchange exchange);
}
