package org.glue.fuse.common.interfaces;

import org.apache.camel.Exchange;
import org.glue.fuse.common.exception.ReflectiveVisitorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * This is an example of a reflective visitor. An example of which can be found here:
 * @Link https://sourcemaking.com/design_patterns/visitor/java/2
 * @author Dan Miles
 */
public abstract class ExchangeEnrichedVisitor {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    private Exchange exchange;

    public Exchange getExchange() {
        return exchange;
    }

    public void setExchange(Exchange exchange) {
        this.exchange = exchange;
    }

    abstract public void visit(Object object);

    protected Method getMethod(Class source){
        Class someClass = source;
        Method methodName = null;
        while (methodName == null && someClass != Object.class){
            String className = someClass.getName();
            className = "visit" + className.substring(className.lastIndexOf('.') + 1);
            try{
                methodName = getClass().getMethod(className, someClass);
            } catch (NoSuchMethodException e) {
                someClass = someClass.getSuperclass();
            }
        }
        if (someClass == Object.class) {
            LOGGER.debug("Searching for class");
            Class[] interfaces = source.getInterfaces();
            for (Class intface : interfaces) {
                String interfaceName = intface.getName();
                interfaceName = "visit" + interfaceName.substring(interfaceName.lastIndexOf('.') + 1);
                try {
                    methodName = getClass().getMethod(interfaceName, intface);
                } catch (NoSuchMethodException ex) {
                    LOGGER.debug(ex.getLocalizedMessage());
                }
            }
        }
        if (methodName == null){
            try {
                methodName = getClass().getMethod("visitObject", Object.class);
            } catch (NoSuchMethodException e) {
                LOGGER.debug(e.getLocalizedMessage());
            }
        }

        return methodName;
    }

    public void visitObject(Object o) throws ReflectiveVisitorException {
        throw new ReflectiveVisitorException("Failed to find reflective visitor method in: " + o.toString());
    }
}
