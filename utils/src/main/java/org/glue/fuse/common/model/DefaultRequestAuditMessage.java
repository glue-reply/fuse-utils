package org.glue.fuse.common.model;

import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.Date;

/**
 * Simple POJO model for Audit Messages
 * @author Dan Miles
 */
@Component
public class DefaultRequestAuditMessage implements org.glue.fuse.common.interfaces.AuditMessage {

    private String accessNetwork;
    private OffsetDateTime apiTime; //TimestampGenerator
    private String ipAddress;
    private String requestUrl; //HTTPServletRequest
    private String sessionId; //HTTPServletRequest
    private String userAgent;
    private String userName; //HTTPServletRequest ??
    private Date userTime;
    private String requestBody; // SelfBuiltMessage / SoapBuilder / Generic


    public String getAccessNetwork() {
        return accessNetwork;
    }

    public void setAccessNetwork(String accessNetwork) {
        this.accessNetwork = accessNetwork;
    }

    public OffsetDateTime getApiTime() {
        return apiTime;
    }

    public void setApiTime(OffsetDateTime apiTime) {
        this.apiTime = apiTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getUserTime() {
        return userTime;
    }

    public void setUserTime(Date userTime) {
        this.userTime = userTime;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public DefaultRequestAuditMessage(){
    }

    @Override
    public Boolean isBuilt(){
        if (this.accessNetwork == null) return false;
        if (this.apiTime == null) return false;
        if (this.ipAddress == null) return false;
        if (this.requestBody == null) return false;
        if (this.requestUrl == null) return false;
        if (this.sessionId == null) return false;
        if (this.userAgent == null) return false;
        if (this.userName == null) return false;
        if (this.userTime == null) return false;
        return true;
    }

}
